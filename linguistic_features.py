import re
import math
import os.path
import itertools
import numpy as np
#from sklearn import svm
#from sklearn import metrics
import collections
from collections import Counter
#import matplotlib.pyplot as plt
#from sklearn.svm import LinearSVC
#from sklearn import preprocessing
from collections import OrderedDict
#from svm import *
#from sklearn.metrics import make_scorer
#from sklearn.model_selection import KFold, cross_validate
import weka.core.jvm as jvm
from weka.classifiers import Classifier, Evaluation
from weka.core.classes import Random, from_commandline
import weka.core.converters as converters
from weka.core.converters import Loader
from weka.core.dataset import Instances
from weka.classifiers import Classifier, SingleClassifierEnhancer, MultipleClassifiersCombiner, FilteredClassifier, PredictionOutput, Kernel, KernelClassifier
from weka.filters import Filter


FOLDERS = ["Attualita", "Cultura", "Sport", "Trento", "Economia"]
MANIFESTO = "Manifesto/Editoriali"
WIKIPEDIA = "Wikipedia"
AMAZON = "Amazon"
VOS_FILE = "VoS.txt"
SENT_LEXICON = "sentix"
#Sizes = [87,73,123,189,54,2243]
Sizes = [87,73,123,189,54,526]

#USE = ["ling_features", "pos_tagging", "unigrams"]

# Italian connectives (from hlt-nlp.fbk.eu/technologies/lico)
TEMPORAL = ["allora","come","dopo","e","frattanto","intanto","mentre","poi","precedentemente","prima","quando","quindi"]
CAUSAL = ["allora","così","dunque","per","perchè","pertanto","poichè","quindi","siccome"]
CONTRASTIVE = ["bensì","invece","ma","mentre","però","viceversa"]
EXPANSIVE = ["anche","anzi","anzichè","cioè","comunque","e","eccetto","fuorchè","infatti","mentre","nè","o","oltretutto","oppure","ossia","persino","piuttosto","pure","tranne"]
# Tagset
TAGLIST = ["ABR","ADJ","ADV","CON","DET:def","DET:indef","FW","INT","LS","NOM","NPR","NUM","PON","PRE","PRE:det","PRO","PRO:demo","PRO:indef","PRO:inter","PRO:pers","PRO:poss","PRO:refl","PRO:rela","SENT","SYM","VER:cimp","VER:cond","VER:cpre","VER:futu","VER:geru","VER:impe","VER:impf","VER:infi","VER:pper","VER:ppre","VER:pres","VER:refl:infi","VER:remo"	]
# List of linguistic features (to print the scores)
FEATURES = ["SentLength","TokenLength","Negations", "Complexity", "Questions", "Exclamations", "Commas", "Semicolons", "Citations", "Interjections", "Verbs", "Pronouns", "fPronouns", "sPronouns", "fsPronouns", "VoS", "Modals","Past","Present","Future", "CondImp", "Conditional", "ConditionalPres", "Gerund", "Imperative", "Imperfect", "Infinitive","PastPerfect","PresPerfect","CausalConn", "TemporalConn", "ContrastiveConn", "ExpansiveConn", "Sent_Pos", "Sent_Neg", "Sent_Pol", "Sent_Int", "Sent_adj_Pos", "Sent_adj_Neg", "Sent_adj_Pol", "Sent_adj_Int"]

def get_features(path, name, lexicon, VOS):
	
	name_text = path + "/" + name + ".txt"
	name_tag = path + "_tag/" + name + ".tagged"
	f_text = open(name_text, "r",encoding="utf-8")
	f_tag = open(name_tag, "r",encoding="utf-8")
	text = f_text.read()

	words = re.sub("[^\w]", " ",  text).split()
	digits = re.findall("[\d]",text)
	Digits = len(digits)/len(text)
	# list of quotation between brackets
	brackets = re.findall("«[^»]+»", text)

	# list of tagged words all and only outside quotes
	words_and_tags = []
	wt_outside_quotes = []
	inside_quotes = False
	for line in f_tag:
		line = line[:-1]
		word, pos, lemma = line.split("\t")
		words_and_tags.append((word,pos,lemma))
		if word=="«":
			inside_quotes = True
		if not inside_quotes:
			wt_outside_quotes.append((word,pos,lemma))
		if word=="»":
			inside_quotes = False
	n = len(words_and_tags)
	n_out = len(wt_outside_quotes)

	Negations = text.count("non")/n
	Questions = text.count("?")/n
	Exclamations = text.count("!")/n
	Commas = text.count(",")/n
	Semicolons = text.count(";")/n

	Citations = sum([len(c) for c in brackets])/len(words_and_tags)

	# sentence related linguistic features
	dots = [pos for pos, tag in enumerate(words_and_tags) if tag[0]=="."]
	sentences = [words_and_tags[s+1:e] for s, e in zip([-1] + dots, dots + [len(words_and_tags)]) if e-s-1>0]

	SentLength = 1/(sum([len(s) for s in sentences])/len(sentences))
	TokenLength = 1/(sum([len(word) for word, tag, label in words_and_tags])/n)

	finite_verbs = []
	for s in sentences:
		finite = [(word, pos, lemma) for (word, pos, lemma) in s if re.match("VER(.)+",pos) and not pos=="VER:infi"]
		ratio = len(finite)/len(s)
		finite_verbs.append(ratio)
	Complexity = sum(finite_verbs)/len(finite_verbs)

	Interjections_lst = [word for (word, pos, lemma) in words_and_tags if lemma == "INT"]
	Interjections = len(Interjections_lst)/n

	# verbs features
	Verbs_lst = [(word, pos, lemma) for (word, pos, lemma) in wt_outside_quotes if re.match("VER(.)+",pos)]
	Verbs = len(Verbs_lst)/n_out
	Modals_lst =  [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if lemma in ["potere", "volere", "dovere"]]
	Modals = len(Modals_lst)/n_out
	Future_lst =  [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:futu"]
	Future = len(Future_lst)/n_out
	Present_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:pres"]
	Present = len(Present_lst)/n_out
	Past_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:remo"]
	Past = len(Past_lst)/n_out
	Cimp_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:cimp"]
	Cimp = len(Cimp_lst)/n_out
	Cond_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:cond"]
	Cond = len(Cond_lst)/n_out
	Cpre_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:cpre"]
	Cpre = len(Cpre_lst)/n_out
	Geru_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:geru"]
	Geru = len(Geru_lst)/n_out
	Impe_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:impe"]
	Impe = len(Impe_lst)/n_out
	Impf_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:impf"]
	Impf = len(Impf_lst)/n_out
	Infi_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:infi"]
	Infi = len(Infi_lst)/n_out
	Pper_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:pper"]
	Pper = len(Pper_lst)/n_out
	Ppre_lst = [(word, pos, lemma) for (word, pos, lemma) in Verbs_lst if pos=="VER:ppre"]
	Ppre = len(Ppre_lst)/n_out
	
	# Verbs of speech
	VoS_lst = [(word, pos, lemma) for (word, pos, lemma) in wt_outside_quotes if lemma in VOS]
	VoS = len(VoS_lst)/n_out

	# pronouns features
	Pronouns_lst = [(word, pos, lemma) for (word, pos, lemma) in wt_outside_quotes if re.match("PRO(.)+",pos)]
	Pronouns = len(Pronouns_lst)/n_out
	fPronouns_lst = [(word, pos, lemma) for (word, pos, lemma) in Pronouns_lst if word=="io"]
	fPronouns = len(fPronouns_lst)/n_out
	sPronouns_lst = [(word, pos, lemma) for (word, pos, lemma) in Pronouns_lst if word=="tu"]
	sPronouns = len(sPronouns_lst)/n_out
	fsPronouns_lst = fPronouns_lst + sPronouns_lst
	fsPronouns = len(fsPronouns_lst)/n_out

	# connective features
	TemporalConn = len([word for word, tag, lemma in words_and_tags if word in TEMPORAL])/len(words_and_tags)
	CausalConn = len([word for word, tag, lemma in words_and_tags if word in CAUSAL])/len(words_and_tags)
	ContrastiveConn = len([word for word, tag, lemma in words_and_tags if word in CONTRASTIVE])/len(words_and_tags)
	ExpansiveConn = len([word for word, tag, lemma in words_and_tags if word in EXPANSIVE])/len(words_and_tags)

	# Sentiment analysis
	#polar_words = [(key, vals) for (key, vals) in lexicon.items() if key in [w for (w,p,l) in wt_outside_quotes]]
	pos_sum, neg_sum, pol_sum, int_sum, adj_pos_sum, adj_neg_sum, adj_pol_sum, adj_int_sum = 0, 0, 0, 0, 0, 0, 0, 0
	for (w,p,l) in wt_outside_quotes:
		if l in lexicon.keys():
			pos_sum += lexicon[l][0]
			neg_sum += lexicon[l][1]
			pol_sum += lexicon[l][2]
			int_sum += lexicon[l][3]
			if p=="ADJ":
				adj_pos_sum += lexicon[l][0]
				adj_neg_sum += lexicon[l][1]
				adj_pol_sum += lexicon[l][2]
				adj_int_sum += lexicon[l][3]
	pos_avg = pos_sum/n_out
	neg_avg = neg_sum/n_out
	pol_avg = pol_sum/n_out
	int_avg = int_sum/n_out

	adj_pos_avg = adj_pos_sum/n_out
	adj_neg_avg = adj_neg_sum/n_out
	adj_pol_avg = adj_pol_sum/n_out
	adj_int_avg = adj_int_sum/n_out

	Sentiment = [pos_avg, neg_avg, pol_avg, int_avg]
	Sentiment_adj = [adj_pos_avg, adj_neg_avg, adj_pol_avg, adj_int_avg]

	f_text.close()
	f_tag.close()

	ling_features = [SentLength, TokenLength, Negations, Complexity, Questions, Exclamations, Commas, Semicolons, Citations, Interjections, Verbs, Pronouns, fPronouns, sPronouns, fsPronouns, VoS, Modals,Past,Present,Future,Cimp,Cond,Cpre,Geru,Impe,Impf,Infi,Pper,Ppre,CausalConn, TemporalConn, ContrastiveConn, ExpansiveConn] + Sentiment + Sentiment_adj

	# tags frequency
	tags_freq = []
	tags = [t for w,t,l in words_and_tags]
	counts = Counter(tags)
	tag_counts = []
	for t in TAGLIST:
		tag_counts += [counts[t]/n]

	# unigrams frequency
	unigrams = [w for w,t,l in words_and_tags if len(w)>1]
	word_counts = OrderedDict({k:v for k, v in Counter(unigrams).items() if v > 5})

	return((ling_features,tag_counts,word_counts))

# combine features into one vector according to USE specification
def combine(edit_ling_feat,news_ling_feat,edit_tags,news_tags,edit_unigrams,news_unigrams,LAB_F,LAB_T,LAB_U, use):
	flatten = lambda l: [item for sublist in l for item in sublist]
	if "ling_features" in use:
		if "pos_tagging" in use:
			if "unigrams" in use:
				edit_feat = [flatten(list(feat)) for feat in zip(edit_ling_feat,edit_tags,edit_unigrams)]
				news_feat = [flatten(list(feat)) for feat in zip(news_ling_feat,news_tags,news_unigrams)]
				labels = LAB_F + LAB_T + LAB_U
			else:
				edit_feat = [flatten(list(feat)) for feat in zip(edit_ling_feat,edit_tags)]
				news_feat = [flatten(list(feat)) for feat in zip(news_ling_feat,news_tags)]
				labels = LAB_F + LAB_T
		elif "unigrams" in use:
			edit_feat = [flatten(list(feat)) for feat in zip(edit_ling_feat,edit_unigrams)]
			news_feat = [flatten(list(feat)) for feat in zip(news_ling_feat,news_unigrams)]
			labels = LAB_F + LAB_U
		else:
			edit_feat = edit_ling_feat
			news_feat = news_ling_feat
			labels = LAB_F
	elif "pos_tagging" in use:
		if "unigrams" in use:
			edit_feat = [flatten(list(feat)) for feat in zip(edit_tags,edit_unigrams)]
			news_feat = [flatten(list(feat)) for feat in zip(news_tags,news_unigrams)]
			labels = LAB_T + LAB_U
		else:
			edit_feat = edit_tags
			news_feat = news_tags
			labels = LAB_T
	else:
		edit_feat = edit_unigrams
		news_feat = news_unigrams
		labels = LAB_U
	return (edit_feat,news_feat,labels)

def save_features(use):

	# sentiemnt lexicon
	f_sent = open(SENT_LEXICON, "r")
	lexicon = {}
	for line in f_sent:
		lemma, POS, sid, pos, neg, pol, inty = line.split()
		if "_" not in lemma:
			lexicon[lemma] = (float(pos),float(neg),float(pol),float(inty))
	f_sent.close()

	f_vos = open(VOS_FILE, "r")
	VOS = []
	for line in f_vos:
		VOS.append(line[:-1])

	# dictionary of words for unigrams frequency ratio
	all_words = Counter()
	
	
	# words from amazon
	for i in range(1,61):
		feat, tags, w_count = get_features(AMAZON, str(i),lexicon,VOS)
		all_words += dict.fromkeys(w_count, 1)
	  
	# words from Wikipedia 
	for i in range(1,61):
		feat, tags, w_count = get_features(WIKIPEDIA, str(i),lexicon,VOS)
		all_words += dict.fromkeys(w_count, 1)
		
	# features vectors from news
	news_ling_feat = []
	news_tags = []
	news_uni_counts = []
	news_labels = []
	for i, fold in enumerate(FOLDERS):
		path = "Adige/"+ fold
		for sample in range(1,Sizes[i]):
			#print(path,sample)
			try:
				feat, tags, w_count = get_features(path, str(sample),lexicon,VOS)
				news_ling_feat += [feat]
				news_tags += [tags]
				news_uni_counts += [w_count]
				all_words += w_count # update unigrams counts
				news_labels += [1]
			except UnicodeDecodeError as e: pass
	
	# features vectors from editorials
	edit_ling_feat = []
	edit_tags = []
	edit_uni_counts = []
	edit_labels = []
	for i in range(1,Sizes[5]):
		#print("Editorial",i)
		feat, tags, w_count = get_features(MANIFESTO, str(i),lexicon,VOS)
		edit_ling_feat += [feat]
		edit_tags += [tags]
		edit_uni_counts += [w_count]
		all_words += w_count
		edit_labels += [0]
	
	
	# add to features relative frequency of unigrams
	news_unigrams = []
	for c in news_uni_counts:
		ratio = []
		for word in all_words:
			#print(word)
			if word in c:
				ratio += [c[word]/all_words[word]]
			else:
				ratio += [0]
		news_unigrams += [ratio]

	edit_unigrams = []
	for c in edit_uni_counts:
		ratio = []
		for word in all_words:
			if word in c:
				ratio += [c[word]/all_words[word]]
			else:
				ratio += [0]
		edit_unigrams += [ratio]

	LAB_U = []
	for word in all_words:
		LAB_U += ["WORD:" + word]

	LAB_F = ["LING:" + f for f in FEATURES]
	LAB_T = ["POS:"+ t for t in TAGLIST]

	edit_feat, news_feat, features_lab = combine(edit_ling_feat,news_ling_feat,edit_tags,news_tags,edit_unigrams,news_unigrams, LAB_F, LAB_T, LAB_U, use)		
		  
	# write arff file for weka classifier
	edit_feat = [l+["editorial"] for l in edit_feat]
	news_feat = [l+["news"] for l in news_feat]
	features = edit_feat + news_feat
	labels = edit_labels + news_labels
	arff_header = "@RELATION genre\n"
	for f in features_lab:
		arff_header+= "@ATTRIBUTE \""+f+"\" REAL\n"
	arff_header+= "@ATTRIBUTE class {editorial, news}\n"
	arff_header+= "@DATA\n"
	arff_format = "%.10f,"*(len(edit_feat[0])-1)+"%s"
	features_file = open("features.arff","w")
	features_file.write(arff_header)
	
	# write each feature per line
	for lst in features:
		features_file.write(",".join(str(f) for f in lst))
		features_file.write("\n")

def test(use, classifier_name, datafile):

	save_features(use)
	scores = {'acc': 'accuracy', 'prec':'precision','rec':'recall','f1':'f1'}


	# load data
	#data = converters.load_any_file("features.csv")
	loader = Loader(classname="weka.core.converters.ArffLoader")
	data = loader.load_file(datafile)
	data.class_is_last()


	classifier = get_classifier(classifier_name)

	# randomize data
	folds = 10
	seed = 1
	rnd = Random(seed)
	rand_data = Instances.copy_instances(data)
	rand_data.randomize(rnd)
	if rand_data.class_attribute.is_nominal:
		rand_data.stratify(folds)

	# perform cross-validation and add predictions
	predicted_data = None
	evaluation = Evaluation(rand_data)
	for i in range(folds):
		train = rand_data.train_cv(folds, i)
		# the above code is used by the StratifiedRemoveFolds filter,
		# the following code is used by the Explorer/Experimenter
		# train = rand_data.train_cv(folds, i, rnd)
		test = rand_data.test_cv(folds, i)

		# build and evaluate classifier
		cls = Classifier.make_copy(classifier)
		cls.build_classifier(train)
		evaluation.test_model(cls, test)

		# add predictions
		addcls = Filter(
			classname="weka.filters.supervised.attribute.AddClassification",
			options=["-classification", "-distribution", "-error"])
		# setting the java object directory avoids issues with correct quoting in option array
		addcls.set_property("classifier", Classifier.make_copy(classifier))
		addcls.inputformat(train)
		addcls.filter(train)  # trains the classifier
		pred = addcls.filter(test)
		if predicted_data is None:
			predicted_data = Instances.template_instances(pred, 0)
		for n in range(pred.num_instances):
			predicted_data.add_instance(pred.get_instance(n))

	print("")
	print("=== Setup ===")
	print("Classifier: " + classifier.to_commandline())
	print("Dataset: " + data.relationname)
	print("Folds: " + str(folds))
	print("Seed: " + str(seed))
	print("")
	print(evaluation.summary("=== " + str(folds) + " -fold Cross-Validation ==="))
	print("")
	print("accuracy: " + str(evaluation.percent_correct))
	print("precision: " + str(evaluation.precision(1)))
	print("recall: " + str(evaluation.recall(1)))
	print("f-score: " + str(evaluation.f_measure(1)))
	print("\n ----------------------\n")

	# output weights
	if classifier_name=="Linear":
		f = "out_weights_"+ classifier_name +".txt"
		out_weights = open(f, "w")
		classifier.build_classifier(data)
		print(classifier,file=out_weights)
		out_weights.close()
		beautify_weights(f)

	# classifiers: linear and polynomial kernels
	#wt = sum(Sizes[0:5])/Sizes[5]
	#clf_linear = svm.SVC(kernel='linear',gamma=2,cache_size=500, class_weight={1: wt})
	#random_state = np.random.RandomState(0)
	#clf_linear = LinearSVC(random_state=random_state, class_weight='balanced')
	#clf_poly = svm.SVC(kernel='poly',degree=2, cache_size=1000, random_state=random_state, class_weight='balanced')


	#linear_scores = cross_validate(clf_linear, features, labels, cv=10, scoring=scores)
	#print("acc:",linear_scores['test_acc'])
	#print("prec:",linear_scores['test_prec'])
	#print("rec:",linear_scores['test_rec'])
	'''
	print("Linear accuracy: %0.3f (+/- %0.3f)" % (linear_scores['test_acc'].mean(), linear_scores['test_acc'].std()/math.sqrt(len(linear_scores['test_acc']))))
	print("Linear precision: %0.3f (+/- %0.3f)" % (linear_scores['test_prec'].mean(), linear_scores['test_prec'].std()/math.sqrt(len(linear_scores['test_prec']))))
	print("Linear recall: %0.3f (+/- %0.3f)" % (linear_scores['test_rec'].mean(), linear_scores['test_rec'].std()/math.sqrt(len(linear_scores['test_rec']))))
	print("Linear f-score: %0.3f (+/- %0.3f)" % (linear_scores['test_f1'].mean(), linear_scores['test_f1'].std()/math.sqrt(len(linear_scores['test_f1']))))
	'''

	#poly_scores = cross_validate(clf_poly, features, labels, cv=10, scoring=scores)
	#print("acc:",poly_scores['test_acc'])
	#print("prec:",poly_scores['test_prec'])
	#print("rec:",poly_scores['test_rec'])

	'''
	print("Polynomial accuracy: %0.3f (+/- %0.3f)" % (poly_scores['test_acc'].mean(), poly_scores['test_acc'].std()/math.sqrt(len(poly_scores['test_acc']))))
	print("Polynomial precision: %0.3f (+/- %0.3f)" % (poly_scores['test_prec'].mean(), poly_scores['test_prec'].std()//math.sqrt(len(poly_scores['test_prec']))))
	print("Polynomial recall: %0.3f (+/- %0.3f)" % (poly_scores['test_rec'].mean(), poly_scores['test_rec'].std()//math.sqrt(len(poly_scores['test_rec']))))
	print("Polynomial f-score: %0.3f (+/- %0.3f)" % (poly_scores['test_f1'].mean(), poly_scores['test_f1'].std()/math.sqrt(len(poly_scores['test_f1']))))
	'''

	'''
	print("Linear kernel weights")
	clf_linear.fit(features,labels)
	weights_linear = list(zip(features_lab,clf_linear.coef_[0]))
	weights_linear.sort(key=lambda x: x[1])
	for f, w in weights_linear[0:15]:
		print(f,":",w)
	for f, w in weights_linear[-15:]:
		print(f,":",w)
	'''

	# barplot of the weights
	'''
	fig = plt.figure()
	ax = fig.add_subplot(111)
	weights = [w for f,w in weights_linear][0:10]+[w for f,w in weights_linear][-10:]
	ind = np.arange(len(weights))
	print(weights)
	width = 0.45
	rects = ax.bar(ind, weights , width, color='black')
	ax.set_xlim(-width,len(ind)+width)
	ax.set_ylim(min(weights),max(weights))
	ax.set_ylabel('Weights')
	ax.set_title('Features weights')
	xTickMarks = [f for f,w in weights_linear[-10:]]+[f for f,w in weights_linear[0:10]]
	ax.set_xticks(ind+width)
	xtickNames = ax.set_xticklabels(xTickMarks)
	plt.setp(xtickNames, rotation=90, fontsize=10)
	plt.show()
	'''
def split_line(line):
	weight, feature = line[1:].split(" * (normalized) ")
	return feature[:-1], float(weight[1:])

def beautify_weights(file_name):

	f = open(file_name,"r")
	feature_list = []
	for line in f:
		if "*" in line:
			feature_list += [split_line(line)]

	ordered = sorted(feature_list, key = lambda tup: tup[1])
	n = len(ordered)
	print("Editorials:\n")
	for i in range(15):
		#print(ordered[i][0],": ",ordered[i][1])
		print("\\textsc{",ordered[i][0],"} & ",ordered[i][1], "\\\\ \\hline")
	print("\n News:\n")
	for i in range(n-1,n-16,-1):
		#print(ordered[i][0],": ",ordered[i][1])
		print("\\textsc{",ordered[i][0],"} & ",ordered[i][1], "\\\\ \\hline")

def test_amz_wiki(use,classifier_name,datafile):
	
	# sentiemnt lexicon
	f_sent = open(SENT_LEXICON, "r")
	lexicon = {}
	for line in f_sent:
		lemma, POS, sid, pos, neg, pol, inty = line.split()
		if "_" not in lemma:
			lexicon[lemma] = (float(pos),float(neg),float(pol),float(inty))
	f_sent.close()

	f_vos = open(VOS_FILE, "r")
	VOS = []
	for line in f_vos:
		VOS.append(line[:-1])

	# dictionary of words for unigrams frequency ratio
	all_words = Counter()
	
	# count words from news
	for i, fold in enumerate(FOLDERS):
		path = "Adige/"+ fold
		for sample in range(1,Sizes[i]):
			try:
				feat, tags, w_count = get_features(path, str(sample),lexicon,VOS)
				all_words += dict.fromkeys(w_count, 1) # update unigrams counts
			except UnicodeDecodeError as e: pass

	# count words from editorials
	for i in range(1,Sizes[5]):
		#print("Editorial",i)
		feat, tags, w_count = get_features(MANIFESTO, str(i),lexicon,VOS)
		all_words += dict.fromkeys(w_count, 1)
	
	
	# features from Amazon reviews
	amz_ling_feat = []
	amz_tags = []
	amz_uni_counts = []
	amz_labels = []
	for i in range(1,61):
		feat, tags, w_count = get_features(AMAZON, str(i),lexicon,VOS)
		amz_ling_feat += [feat]
		amz_tags += [tags]
		amz_uni_counts += [w_count]
		all_words += w_count
		amz_labels += [0]
	  
	# features from Wikipedia reviews
	wiki_ling_feat = []
	wiki_tags = []
	wiki_uni_counts = []
	wiki_labels = []
	for i in range(1,61):
		feat, tags, w_count = get_features(WIKIPEDIA, str(i),lexicon,VOS)
		wiki_ling_feat += [feat]
		wiki_tags += [tags]
		wiki_uni_counts += [w_count]
		all_words += w_count
		wiki_labels += [1]

	
	wiki_unigrams = []
	for c in wiki_uni_counts:
		ratio = []
		for word in all_words:
			if word in c:
				ratio += [c[word]/all_words[word]]
			else:
				ratio += [0]
		wiki_unigrams += [ratio]

	amz_unigrams = []
	for c in amz_uni_counts:
		ratio = []
		for word in all_words:
			if word in c:
				ratio += [c[word]/all_words[word]]
			else:
				ratio += [0]
		amz_unigrams += [ratio]

	LAB_U = []
	for word in all_words:
		LAB_U += ["WORD:" + word]

	LAB_F = ["LING:" + f for f in FEATURES]
	LAB_T = ["POS:"+ t for t in TAGLIST]
	
	
	amz_feat, wiki_feat, features_lab = combine(amz_ling_feat,wiki_ling_feat,amz_tags,wiki_tags,amz_unigrams,wiki_unigrams, LAB_F, LAB_T, LAB_U, use)		
	
	amz_feat = [l+["editorial"] for l in amz_feat]
	wiki_feat = [l+["news"] for l in wiki_feat]
	features = amz_feat + wiki_feat
	labels = amz_labels + wiki_labels
	arff_header = "@RELATION genre\n"
	for f in features_lab:
		arff_header+= "@ATTRIBUTE \""+f+"\" REAL\n"
	arff_header+= "@ATTRIBUTE class {editorial, news}\n"
	arff_header+= "@DATA\n"
	arff_format = "%.10f,"*(len(amz_feat[0])-1)+"%s"
	
	features_file = open("features_aw.arff","w")
	features_file.write(arff_header)
	
	# write each feature per line
	for lst in features:
		features_file.write(",".join(str(f) for f in lst))
		features_file.write("\n")

	save_features(use)
	
	# load data
	loader = Loader(classname="weka.core.converters.ArffLoader")
	train = loader.load_file("features.arff")
	train.class_is_last()
	test = loader.load_file("features_aw.arff")
	test.class_is_last()
	data = loader.load_file(datafile)
	data.class_is_last()

	classifier = get_classifier(classifier_name)

	classifier.build_classifier(train)
	evaluation = Evaluation(data)
	evaluation.test_model(classifier, test)

	print(evaluation.summary("==== Amazon/Wikipedia ===="))
	print("")
	print("accuracy: " + str(evaluation.percent_correct))
	print("precision: " + str(evaluation.precision(1)))
	print("recall: " + str(evaluation.recall(1)))
	print("f-score: " + str(evaluation.f_measure(1)))
	print("\n ----------------------\n")	

def test_dom_robustness(use,classifier_name,datafile):
	group = {
		0: "train",
		1: "test",
		2: "train",
		3: "test",
		4: "train",
	}
	#save_features(use)
	f = open(datafile, "r")
	test = ""
	train = ""
	n_line = 0
	offset = -1
	add_editorial = True
	for line in f:
		n_line+=1
		if re.search("@",line):
			test+=line
			train+=line
		elif re.search("news\n",line):
			if offset is -1:
				offset= n_line
			grp = which_class(offset,n_line)
			if group[grp] is "test":
				test+=line
			else:
				train+=line
		elif re.search("editorial\n",line):
			if add_editorial:
				train+=line
			else:
				test+=line
			add_editorial= not add_editorial
		else:
			pass
	'''
	for line in f:
		n_line+=1
		if re.search("class", line):
			data+="@ATTRIBUTE class {groupA, groupB}\n"
		elif re.search("editorial\n",line):
			pass
		elif re.search("news\n",line):
			if offset is -1:
				offset= n_line
			trim = len(line)-5
			newline = line[0:trim]
			if (n_line-offset)<Sizes[0]:
				newline+=group[0]
			elif (n_line-offset-Sizes[0])<Sizes[1]:
				newline+=group[1]
			elif (n_line-offset-Sizes[0]-Sizes[1])<Sizes[2]:
				newline+=group[2]
			elif (n_line-offset-Sizes[0]-Sizes[1]-Sizes[2])<Sizes[3]:
				newline+=group[3]
			else:
				newline+=group[4]
			data+=newline
			data+="\n"
		else:
			data+=line
	'''
	features_file = open("features_dom_test.arff","w")
	features_file.write(test)
	features_file = open("features_dom_train.arff","w")
	features_file.write(train)
	# load data
	loader = Loader(classname="weka.core.converters.ArffLoader")
	train = loader.load_file("features_dom_train.arff")
	train.class_is_last()
	test = loader.load_file("features_dom_test.arff")
	test.class_is_last()
	data = loader.load_file(datafile)
	data.class_is_last()

	classifier = get_classifier(classifier_name)

	classifier.build_classifier(train)
	evaluation = Evaluation(data)
	evaluation.test_model(classifier, test)

	print(evaluation.summary("==== Domain robustness ===="))
	print("")
	print("accuracy: " + str(evaluation.percent_correct))
	print("precision: " + str(evaluation.precision(1)))
	print("recall: " + str(evaluation.recall(1)))
	print("f-score: " + str(evaluation.f_measure(1)))
	print("\n ----------------------\n")


def which_class(offset,current):
	n = current-offset
	clss = 0
	for i in range(0,5):
		base= 0
		top= 0
		for b in range(0,i):
			base+=Sizes[b]
		for t in range(0,i+1):
			top+=Sizes[t]
		if base<n and n<top:
			clss= i
	return clss

def get_classifier(classifier_name):
	# classifier
	if classifier_name=="Tree":
		return Classifier(classname="weka.classifiers.trees.J48", options=["-C", "0.3"])
	elif classifier_name=="NaiveBayes":
		return Classifier(classname="weka.classifiers.bayes.NaiveBayes")
	elif classifier_name=="Polynomial":
		cmdline = 'weka.classifiers.functions.SMO -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -E 2.0"'
		return from_commandline(cmdline, classname="weka.classifiers.Classifier")
	else:
		cmdline = 'weka.classifiers.functions.SMO -K "weka.classifiers.functions.supportVector.PolyKernel"'
		return from_commandline(cmdline, classname="weka.classifiers.Classifier")


def main():

	classifier = "Polynomial"
	data = "features.arff"
	jvm.start()

	use = ["ling_features"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)

	use = ["pos_tagging"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)


	use = ["unigrams"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)

	use = ["ling_features", "pos_tagging"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)


	use = ["ling_features", "unigrams"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)


	use = ["pos_tagging", "unigrams"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)


	use = ["ling_features", "pos_tagging", "unigrams"]
	print(use)
	test(use,classifier,data)
	#test_dom_robustness(use,classifier,data)
	#test_amz_wiki(use,classifier,data)

	jvm.stop()


if __name__ == "__main__":
	main()