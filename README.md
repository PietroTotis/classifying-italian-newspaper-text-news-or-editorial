# README #
This repository contains the code described in the paper "Classifying Italian newspaper text: news or editorial?":
We present a text classifier that can distinguish Italian news stories from editorials. 
Inspired by earlier work on English, we built a suitable train/test corpus and implemented a range of features, which can predict the distinction with an accuracy of 89,12%. 
As demonstrated by the earlier work, such a feature-based approach outperforms simple bag-of-words models when being transferred to new domains. 
We argue that the technique can also be used to distinguish opinionated from non-opinionated text outside of the realm of newspapers. 